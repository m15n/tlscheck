import requests

# https://hostname or IP is input

def verify_uri_connection(address):
    try:
        req = requests.get(address)

        if req.status_code == 200:
            if not req.history:
                print("all good!")
            elif req.history:
                print("was redirected")
            else:
                print("You somehow hit else.")
        else:
            print("you hit else. here is req:" + str(req))

    except requests.exceptions.ConnectTimeout:
        print("Connection timed out. Perhaps not reachable, or TLS not enabled?")

    except requests.exceptions.ConnectionError:
        print("Connection failed.")

address = "http://m15n.no"


verify_uri_connection(address)
