#!/usr/bin/env python3
#from sqlalchemy import SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
#from sqlalchemy.orm import Column, Integer, Tables
from sqlalchemy.orm import declarative_base, relationship
import app

import config
import sqlalchemy as db
from datetime import datetime
import protect_password

engine = db.create_engine('sqlite:///tlscheck.sqlite')
connection = engine.connect
metadata = db.MetaData()
#tlscheck = db.Table('tlscheck', metadata, autoload=True, autoload_with=engine)
Base = declarative_base()
db = SQLAlchemy(app)


class Urls(url, Base):
    __tablename__ = "urls"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    url = db.Column(db.String, nullable=False)
    ca_org = db.Column(db.String(255))
    ca_issuer = db.Column(db.String(255))
    expiration_date = db.Column(db.DateTime)

class Overview(Base):
    __tablename__ = "overviews"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    url = db.relationship("Urls", back_populates="overviews")
    user_id = db.relationship("Users", back_populates="overviews")

class Users(username, password, email, Base):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    password = db.Column('password', db.String(150), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False)
    updated_on = db.Column(db.DateTime)
    last_logged_in = db.Column(db.DateTime)
    username = db.Column(db.String(150), nullable=False)
    email = db.Column(db.String(150))

    def _set_password(self, password):
        self._password = protect_password(password)


Base.metadata.create_all(engine)

