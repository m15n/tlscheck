#!/usr/bin/env python3

from flask import Flask, render_template, request
import config

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/login', methods = ['POST'])
def login():
    return render_template("login.html")

@app.route('/user')
def user():
    return render_template("user.html")

from flask_wtf import FlaskForm
import model
from wtforms import Form

@app.route('/add_url', methods =['GET', 'POST'])
def add_url():
    if request.method == 'POST':
        url = request.form['url']
        new_url = Urls(url=form.url)
        db.sesison.add(new_url)
        return redirect(url_for('/success'))
    else:
        return render_template("add_url.html")


if __name__ == '__main__':
    app.run(host=config.HOST, port=config.PORT)
