

import ssl, socket

host = "eff.org"
ctx = ssl.create_default_context()

with ctx.wrap_socket(socket.socket(), server_hostname=host) as s:
    s.connect((host, 443))
    cert = s.getpeercert()

issuer = dict(x[0] for x in cert['issuer'])

issuer_org = issuer['organizationName']
issuer_cn = issuer['commonName']

print(issuer_org + " " + "(" + issuer_cn + ")")


#print(cert)
