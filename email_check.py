from validate_email import validate_email

input1 = "correct@mail.com"
input2 = "invalid.mail@com"

# haven't decided yet if i actually care whether an email is legit.
# if there is some notification thing, i do, otherwise meh.
# password resets might be handy however.

def check_email(email):
    
    email_address=email,
    check_format=True,
    check_blacklist=True,
    check_dns=True,
    dns_timeout=10
