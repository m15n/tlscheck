import OpenSSL
import ssl, socket
import datetime
from datetime import timedelta, date

# get time until ssl expires
cert = ssl.get_server_certificate(('m15n.no', 443))
x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
x509.get_notAfter()

# probably merge, but un-byting
date_decoded = x509.get_notAfter().decode('UTF-8')

# making an object thats not fucking useless
dt = datetime.datetime.strptime(date_decoded, "%Y%m%d%H%M%SZ")

today = datetime.datetime.now()

delta = dt - today
print(f"Certificate expires in {delta.days}.")

if delta.days > 50:
    print("More than 50 days left.")


