#!/usr/bin/env python3

import requests
from sys import argv
import ssl, socket
import OpenSSL
import datetime
from datetime import timedelta, date

# Verifying that there is something there before checking its certificate
def verify_uri_connection(address):
    if "https://" in address:                  
        pass
    elif "http://" in address:
        address = address.strip("http://")
        address = "https://" + address
    else:
        address = "https://" + address
    try:                                                    # This can help in case of HTTP 445
        req = requests.head(address, allow_redirects=True)#, headers={"User-Agent": "."})
        if req.status_code == 200:
            if not req.history:
                fetch_tls_info(address)
            elif req.history:
                print(f"[*] {address} -> {req.url}")
                fetch_tls_info(address)
                fetch_tls_info(req.url)
        else:
            print(f"Aborting, {address} got HTTP code " + str(req.status_code))
    except Exception:
        pass

def fetch_tls_info(address):
    if "https://" in address:
        address = address.strip("https://")
    elif "http://" in address:
        address = address.strip("http://") 

    # Reading the certificate and pulling issuer info and checking days before expiry
    context = ssl.create_default_context()
    try:
        with context.wrap_socket(socket.socket(), server_hostname=address) as s:
            s.connect((address, 443))
            cert = s.getpeercert()
            issuer = dict(x[0] for x in cert['issuer'])  
            issuer_org = issuer['organizationName']
            issuer_cn = issuer['commonName']
    except socket.gaierror: 
        pass
    except ssl.SSLCertVerificationError:
        pass
    except Exception as e:
        print("Catch-all error: ", e) 
    finally:
        try:
            cert = ssl.get_server_certificate((address, 443))
            x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)
            x509.get_notAfter()
            date_decoded = x509.get_notAfter().decode('UTF-8')
            cert_date = datetime.datetime.strptime(date_decoded, "%Y%m%d%H%M%SZ")
            today = datetime.datetime.now()
            delta = cert_date - today
            if "-" in str(delta.days):
                cert_expired = True
            elif not "-" in str(delta.days):
                cert_expired = False
                days_left = delta.days
            
            if cert_expired: 
                print(f"[+] {address} | expired")
            elif not cert_expired:
                print(f"[+] {address} | {days_left} | {issuer_org} | {issuer_cn}")
        except socket.gaierror:
            print(f"[!] Unable to resolve: {address}")
        except Exception as e:
            print(f"[x] Failed getting expiry date for {address}")

# As opposed to just looping the script itself over a file
def iterate_urls(file):
    with open(argv[2], 'r') as f:
        db = f.read().splitlines()
        f.close()
        for address in db:
            verify_uri_connection(address)

def usage():
    print("""usage:
            ./tlscheck.py 
            -u / --url <url>                            | Check one URL.
            -f / --file <file with one url per line>    | Iterate a list of URLs.
            -h / --help                                 | Display this help text.""")

# A rather mediocre menu.
try:
    if argv[1] == "-u" or argv[1] == "--url":
        address = argv[2]
        verify_uri_connection(address)
    
    elif argv[1] == "-f" or argv[1] == "--file":
        iterate_urls(argv[2])
    
    elif argv[1] == "-h" or argv[1] == "--help":
        usage()
    else:
        print("else")
        usage()
except IndexError:
    usage()
