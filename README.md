## Status:
 As of now, the cli/tlscheck.py largely functions as desired.
I will be keeping the rest of the code, but I like the cli version.

The plan changed, but it's at least something semi-functional now.

Known issues:
- Sometimes getting HTTP 405, sometimes seemingly fixed by setting another user agent.
- Not if the hostname search for covered by a wildcard cert, it can/will fail.
- If the hostname has a weird redirect, the resolving might fail (eg. redir to https://whatever:443)

 
------------------------------------------------------
If this ever is finished, it might be kinda cool.

Basic idea:
Clean and compact list of urls, with what CA issues their certs and how long before it expires. Similar stuff exists in many services, but I don't know of any dedicated services. This one doesn't care if the site is down for a week, just about the TLS status. At least that is my initial plan. UI/UX will have a large focus.


## Stack

Front: 
- jQuery
- TailwindCSS?

Back:
- Python
- Flask
- PostgreSQL


## To do


#### Resources so I remember
https://docs.sqlalchemy.org/en/14/orm/backref.html  
https://www.pythoncentral.io/introductory-tutorial-python-sqlalchemy/  
https://towardsdatascience.com/sqlalchemy-python-tutorial-79a577141a91  
https://docs.sqlalchemy.org/en/20/orm/declarative_tables.html  

